package com.photo.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zhangxiaowei 2018/11/12
 */
public class CameraUtil {
    Uri mPhotoUri;

    /**
     * 使用相机拍照
     */
    public String goCamera(Activity mContext) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //以系统时间作为该文件 民命
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String picName = formatter.format(curDate);
//        setPicName(picName);
//        picName = formatter.format(curDate);
        File picture = new File(ImageUtil.getInstance().getCameraPath(), picName + ".jpg");
        //ToastUtils.showShort(file.getPath());

        startActionCapture(mContext, picture);
//        Uri uri = Uri.fromFile(picture);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//        mContext.startActivityForResult(intent, KeyImage.xiang_ji);
        return picName;
    }

    /**
     * 打开相机
     *
     * @param activity Activity
     * @param file     File
     */
    public void startActionCapture(Activity activity, File file) {
        if (activity == null) {
            return;
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getUriForFile(activity, file));
        activity.startActivityForResult(intent, KeyImage.xiang_ji);
    }

    private Uri getUriForFile(Activity context, File file) {
        if (context == null || file == null) {
            throw new NullPointerException();
        }
        Uri uri;
        if (Build.VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile(context.getApplicationContext(),
                    "com.photo.fileprovider", file);
        } else {
            uri = Uri.fromFile(file);
        }
        mPhotoUri = uri;
        return uri;
    }

    /**
     * 点击 加号 去图片网格选择图片
     *
     * @param mContext
     * @param selectedList
     */
//    public void goPhoto(Activity mContext, ArrayList<String> selectedList) {
//        Intent intent = new Intent();
//        intent.setClass(mContext, ImageListActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putInt(KeyImage.PHOTO_COUNTS, KeyImage.default_checked_counts);
//        // ToastUtils.showToastByShort("目前已经选了" + selectedList.size() + "张");
//        selectedList.remove(KeysUtil.KEY1);
//        bundle.putStringArrayList(KeyImage.imageData, selectedList);
//        intent.putExtras(bundle);
//        mContext.startActivityForResult(intent, KeyImage.xiang_ce);
//    }

}
