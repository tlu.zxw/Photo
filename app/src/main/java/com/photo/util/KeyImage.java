package com.photo.util;

public interface KeyImage {
    String imageData = "imageData";
    int default_checked_counts = 5;//默认最多能选中图片的个数
    /**
     * 相机拍照
     */
    int xiang_ji = 800;
    /**
     * 相册选择图片
     */
    int xiang_ce = 600;

    /**
     * 谢绝
     */
    String PHOTO_COUNTS = "counts";

}
