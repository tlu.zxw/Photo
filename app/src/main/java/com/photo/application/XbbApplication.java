package com.photo.application;

import android.app.Application;
import android.content.Context;

/**
 * created by zhangxiaowei on 2018/12/6 11:26 AM
 */
public class XbbApplication extends Application {
    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }
}
